package biz.bigappfromskiff2.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import biz.bigappfromskiff2.R;


public class RestoreActivity extends AppCompatActivity implements View.OnClickListener{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.restore_actyvity);


        Button mButtonCancel = (Button) findViewById(R.id.btn_cancel_restore);
        Button mButtonSave = (Button) findViewById(R.id.btn_save_restore);
        mButtonCancel.setOnClickListener(this);
        mButtonSave.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_cancel_restore:
                Toast.makeText(this, "Cancel is pressed!", Toast.LENGTH_LONG).show();
            case R.id.btn_save_restore:
                Toast.makeText(this, "Save is pressed!", Toast.LENGTH_LONG).show();
                break;
        }
    }
}
