package biz.bigappfromskiff2.models;

import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

/**
 * @author Stas
 * @since 14.03.17.
 */

public class TarikStory implements Serializable {

    @DatabaseField(columnName = "id")
    private int id;
    @DatabaseField(columnName = "firstName")
    private int firstName;
    @DatabaseField(columnName = "lastName")
    private String lastName;
    @DatabaseField(columnName = "profession")
    private String profession;
    @DatabaseField(columnName = "age")
    private int age;

    public TarikStory() {

    }


    public TarikStory(int id, int firstName, String lastName, String prifession, int age) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.profession = prifession;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFirstName() {
        return firstName;
    }

    public void setFirstName(int firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPrifession() {
        return profession;
    }

    public void setPrifession(String prifession) {
        this.profession = prifession;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
