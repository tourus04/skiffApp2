package biz.bigappfromskiff2.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import biz.bigappfromskiff2.models.TarikStory;

/**
 * @author Stas
 * @since 30.08.16.
 */

public class OrmDBHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "agroyard.db";
    private static final int DATABASE_VERSION = 1;


    private Dao<TarikStory, Integer> mStory;


    public OrmDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, TarikStory.class);

            Log.i(getClass().getName(), "TABLE CREATED");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database,
                          ConnectionSource connectionSource,
                          int oldVersion, int newVersion) {

    }


    public Dao<TarikStory, Integer> getStory() throws SQLException {
        if (mStory == null) {
            mStory = getDao(TarikStory.class);
        }
        return mStory;
    }

}

