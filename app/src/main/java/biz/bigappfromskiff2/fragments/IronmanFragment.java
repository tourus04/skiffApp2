package biz.bigappfromskiff2.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import biz.bigappfromskiff2.R;

/**
 * @author Stas
 * @since 10.03.17.
 */

public class IronmanFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.registration_fragment, container, false);

        TextView tv = (TextView) v.findViewById(R.id.tv_ironman);
        tv.setText(getArguments().getString("msg"));

        return v;
    }

    public static IronmanFragment newInstance(String text) {

        IronmanFragment f = new IronmanFragment();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }
}
