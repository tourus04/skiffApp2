package biz.bigappfromskiff2.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import biz.bigappfromskiff2.R;
import biz.bigappfromskiff2.activity.RestoreActivity;

/**
 * @author Stas
 * @since 10.03.17.
 */

public class EnterFragment extends Fragment implements View.OnClickListener {



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.enter_fragment, container, false);

/*        TextView tv = (TextView) v.findViewById(R.id.tv_batman);
        tv.setText(getArguments().getString("msg"));*/

        TextView mTextView = (TextView) v.findViewById(R.id.tv_forgot_pass);
        mTextView.setOnClickListener(this);

        Button mButton = (Button) v.findViewById(R.id.btn_enter_fragment);
        mButton.setOnClickListener(this);

        return v;



    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_forgot_pass:
                Intent mIntent = new Intent(getActivity(), RestoreActivity.class);
                startActivity(mIntent);
            case R.id.btn_enter_fragment:
                Toast.makeText(getActivity(), "Enter is pressed!", Toast.LENGTH_LONG).show();
            break;
        }
    }

    public static EnterFragment newInstance(String text) {

        EnterFragment f = new EnterFragment();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }
}