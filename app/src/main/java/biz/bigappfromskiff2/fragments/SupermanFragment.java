package biz.bigappfromskiff2.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import biz.bigappfromskiff2.R;

/**
 * @author Stas
 * @since 10.03.17.
 */

public class SupermanFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.restore_fragment, container, false);

        TextView tv = (TextView) v.findViewById(R.id.tv_superman);
        tv.setText(getArguments().getString("msg"));

        return v;
    }

    public static SupermanFragment newInstance(String text) {

        SupermanFragment f = new SupermanFragment();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }
}
