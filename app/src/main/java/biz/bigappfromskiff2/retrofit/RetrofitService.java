package biz.bigappfromskiff2.retrofit;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import okhttp3.Authenticator;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author Stas
 * @since 11.08.16.
 */

public class RetrofitService {

    private static API sRetrofitApi;

    private RetrofitService() {}

    /**
     * Instance oj {@link API} used with main thread only;
     * @return instance
     */
    public static API getInstance() throws NoSuchAlgorithmException, KeyManagementException {

        OkHttpClient  client = new OkHttpClient.Builder()
                .authenticator(new Authenticator() {
                    @Override public Request authenticate(Route route, Response response) throws IOException {
                        System.out.println("Authenticating for response: " + response);
                        System.out.println("Challenges: " + response.challenges());
                        String credential = Credentials.basic("admin", "password");
                        return response.request().newBuilder()
                                .header("Authorization", credential)
                                .build();
                    }
                })
                .build();


        Retrofit mRetrofit = new Retrofit.Builder()
                    .baseUrl("https://api.dev.agroyard.com.ua/api/")
//                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();




        return mRetrofit.create(API.class);
    }

    /**
     * Instance oj {@link API} used with custom converter;
     * @return instance
     */
    public static API cusomInstance() {
        Retrofit mRetrofit = new Retrofit.Builder()
                .baseUrl(API.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return mRetrofit.create(API.class);
    }



}
